<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id('pro_id');
            $table->bigInteger('pro_cat_id');
            $table->string('pro_name');
            $table->string('pro_description');
            $table->string('pro_price');
            $table->string('pro_sales_discount');
            $table->tinyInteger('pro_hot');
            $table->tinyInteger('pro_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
