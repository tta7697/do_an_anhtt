<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('ord_id');
            $table->bigInteger('ord_use_id');
            $table->string('ord_shipping_name');
            $table->string('ord_shipping_phone');
            $table->string('ord_shipping_address');
            $table->string('ord_payment_method');
            $table->string('ord_total_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
