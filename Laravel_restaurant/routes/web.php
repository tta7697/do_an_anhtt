<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* View index */
Route::get('/', 'HomeController@index')->name('index');

/* View menu */
Route::get('/menu', 'MenuController@index')->name('menu');

/* View reservation */
Route::get('/reservation', 'ReservationController@index')->name('reservation');

/* View gallery */
Route::get('/gallery', 'GalleryController@index')->name('gallery');

/* View about */
Route::get('/about', 'AboutController@index')->name('about');

/* View blog */
Route::get('/blog', 'BlogController@index')->name('blog');

/* View contact */
Route::get('/contact', 'ContactController@index')->name('contact');


